package com.demo.leave.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.leave.entity.Leaves;
import com.demo.leave.exception.LeaveException;
import com.demo.leave.repository.LeaveRepository;
import com.demo.leave.service.LeaveService;
import com.demo.leave.utility.ErrorConstant;

@Service
public class LeaveServiceImpl implements LeaveService {

	@Autowired
	LeaveRepository leaveRepository;

	@Override
	public List<Leaves> getallleave(int userid) throws LeaveException {
		List<Leaves> leave = leaveRepository.findAllByUser_userId(userid);

		if (!leave.isEmpty()&&leave.get(0).getUser().getUserId()!=0) {

			return leave;
		} else {
			
			
			throw new LeaveException(ErrorConstant.RECORD_NOT_FOUND);
		}

	}
}