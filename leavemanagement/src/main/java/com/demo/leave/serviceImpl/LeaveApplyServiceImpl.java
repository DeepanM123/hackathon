package com.demo.leave.serviceImpl;

import java.time.LocalDate;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.demo.leave.dto.LeaveApplyDto;
import com.demo.leave.entity.Leaves;
import com.demo.leave.entity.UserLeave;
import com.demo.leave.exception.LeaveException;
import com.demo.leave.repository.LeaveApplyRepository;
import com.demo.leave.repository.LeaveRepository;
import com.demo.leave.service.LeaveApplyService;
import com.demo.leave.utility.ErrorConstant;

@Service
public class LeaveApplyServiceImpl implements LeaveApplyService {

	@Autowired
	LeaveRepository leaveRepository;

	@Autowired
	LeaveApplyRepository leaveApplyRepository;

	

	
	@Override
	public String saveLeaveData(@Valid LeaveApplyDto leaveapply) throws LeaveException {

		Optional<Leaves> leave = leaveRepository.findByUser_userIdAndLeaveType(leaveapply.getUserIdfk(),
				leaveapply.getLeaveType());
		if (!leave.isPresent()) {
			throw new LeaveException(ErrorConstant.RECORD_NOT_FOUND);
		}

		if ((leave.get().getAvailableLeaves() > 0)
				//--------------------------------------------------------------------------//
				&& (leave.get().getAvailableLeaves() >= leave.get().getAvailableLeaves())) {

			leave.get().setAvailableLeaves(leave.get().getAvailableLeaves() - leaveapply.getNumberOfLeaves());
			leaveRepository.save(leave.get());
			UserLeave userLeave = new UserLeave();
			BeanUtils.copyProperties(leaveapply, userLeave);
			leaveApplyRepository.save(userLeave);
		}

		return "Applied for leave successfully";
	}

}
