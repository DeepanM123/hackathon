package com.demo.leave.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.leave.entity.UserLeave;

@Repository
public interface LeaveApplyRepository extends JpaRepository<UserLeave, Integer> {

}
