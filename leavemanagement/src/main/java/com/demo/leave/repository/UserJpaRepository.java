package com.demo.leave.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.demo.leave.entity.User;

//@Transactional
@Repository
public interface UserJpaRepository extends JpaRepository<User, Integer> {

	List<User> findByUserNameAndPassword(String username, String password);

}
