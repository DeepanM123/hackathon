package com.demo.leave.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.leave.entity.Leaves;
import com.demo.leave.enums.LeaveType;

@Repository
public interface LeaveRepository extends JpaRepository<Leaves, Integer> {

	List<Leaves> findAllByUser_userId(int userid);

	Optional<Leaves> findByUser_userIdAndLeaveType(int userId, LeaveType leaveType);

}
