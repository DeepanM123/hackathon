package com.demo.leave.exception;

public class ErrorResponse {

	private String message;
	private int statusCode;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public ErrorResponse(String message) {
		super();
		this.message = message;
	}

	public ErrorResponse() {
		super();
	}

}
