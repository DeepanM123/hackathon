package com.demo.leave.Controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.leave.dto.LeaveApplyDto;
import com.demo.leave.dto.LoginDto;
import com.demo.leave.entity.Leaves;
import com.demo.leave.entity.User;
import com.demo.leave.exception.LeaveException;
import com.demo.leave.repository.UserJpaRepository;
import com.demo.leave.service.LeaveApplyService;
import com.demo.leave.service.LeaveService;
import com.demo.leave.serviceImpl.LeaveServiceImpl;
import com.demo.leave.utility.ErrorConstant;

@RestController
public class LeaveController {

	@Autowired
	LeaveService leaveService;

	@Autowired
	LeaveApplyService leaveApplyService;

	@Autowired
	LeaveServiceImpl leaveserviceimpl;
	@Autowired
	UserJpaRepository userRepository;

	@PostMapping("/login")
	public String loginUser(@RequestBody LoginDto loginDto) throws LeaveException {
		List<User> userList = userRepository.findByUserNameAndPassword(loginDto.getUserName(), loginDto.getPassword());

		if (!userList.isEmpty()&&!userList.get(0).getUserName().isEmpty()) {
			return "Log In is successfull";
			
		}

		else
			throw new LeaveException(ErrorConstant.LOGIN_UNSUCCESSFUL);

	}

	@GetMapping("/Users/{userid}")
	public List<Leaves> getallleavesbyuserid(@PathVariable int userid) throws LeaveException {
		leaveserviceimpl.getallleave(userid);
		return leaveService.getallleave(userid);
		
	}



	@PostMapping("/applyleave")
	public ResponseEntity<String> applyLeave(@Valid @RequestBody LeaveApplyDto leaveapply)
			throws LeaveException {
		return new ResponseEntity<String>(leaveApplyService.saveLeaveData(leaveapply), HttpStatus.OK);
	}

	

	

}
