package com.demo.leave.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "UserDetails")
public class User implements Serializable {

	@Id
	@GeneratedValue
	private int userId;
	private String userName;
	private String password;
	private String email;
	private String gender;
	private String phone;

	public User() {

	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userid) {
		this.userId = userid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	Set<Leaves> leave;

	public User(Set<Leaves> leave) {
		super();
		this.leave = leave;
	}

	public Set<Leaves> getLeave() {
		return leave;
	}

	public void setLeave(Set<Leaves> leave) {
		this.leave = leave;
	}

}
