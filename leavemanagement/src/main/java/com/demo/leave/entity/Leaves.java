package com.demo.leave.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.demo.leave.enums.LeaveType;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Table(name = "LeaveDetails")
@Entity
public class Leaves implements Serializable {

	@Id
	@GeneratedValue
	private int leaveId;
	@Enumerated(EnumType.STRING)
	private LeaveType leaveType;
	private int availableLeaves;

	public Leaves() {

	}

	public int getLeaveId() {
		return leaveId;
	}

	public void setLeaveId(int leaveId) {
		this.leaveId = leaveId;
	}

	public LeaveType getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	public int getAvailableLeaves() {
		return availableLeaves;
	}

	public void setAvailableLeaves(int availableLeaves) {
		this.availableLeaves = availableLeaves;
	}

	@ManyToOne
	@JoinColumn(name = "userId")
	User user;

	public Leaves(User user) {
		super();
		this.user = user;
	}

	@JsonIgnore
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
