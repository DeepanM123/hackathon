package com.demo.leave.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.demo.leave.enums.LeaveType;

@Table(name = "UserLeaveDetails")
@Entity
public class UserLeave {

	@Id
	@GeneratedValue
	int id;
	int userIdfk;
	int leaveIdfk;
	@Enumerated(EnumType.STRING)
	private LeaveType leaveType;
	int numberOfLeaves;
	//------------------//localdate
	private Date FromDate;
	private Date ToDate;

	public int getNumberOfLeaves() {
		return numberOfLeaves;
	}

	public void setNumberOfLeaves(int numberOfLeaves) {
		this.numberOfLeaves = numberOfLeaves;
	}

	public Date getFromDate() {
		return FromDate;
	}

	public void setFromDate(Date fromDate) {
		FromDate = fromDate;
	}

	public Date getToDate() {
		return ToDate;
	}

	public void setToDate(Date toDate) {
		ToDate = toDate;
	}

	public LeaveType getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(LeaveType leaveType) {
		this.leaveType = leaveType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserIdfk() {
		return userIdfk;
	}

	public void setUserIdfk(int userIdfk) {
		this.userIdfk = userIdfk;
	}

	public int getLeaveIdfk() {
		return leaveIdfk;
	}

	public void setLeaveIdfk(int leaveIdfk) {
		this.leaveIdfk = leaveIdfk;
	}

	public UserLeave() {

	}

}
