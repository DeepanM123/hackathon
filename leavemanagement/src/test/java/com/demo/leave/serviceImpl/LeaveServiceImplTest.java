package com.demo.leave.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.assertj.core.api.AssertFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.demo.leave.entity.Leaves;
import com.demo.leave.entity.User;
import com.demo.leave.enums.LeaveType;
import com.demo.leave.exception.LeaveException;
import com.demo.leave.repository.LeaveRepository;

@RunWith(MockitoJUnitRunner.class)
public class LeaveServiceImplTest {

	@Mock
	LeaveRepository leaveRepository;

	@InjectMocks
	LeaveServiceImpl leaveServiceImpl;

	List<Leaves> listOfLeaves;

	Leaves leaves;
	LeaveType leaveType;
	int userid;

	User user;

	@Before
	public void setup() {

		leaves = new Leaves();
		leaves.setAvailableLeaves(2);
		leaves.setLeaveId(1);
		leaves.setLeaveType(leaveType.Personal);
		leaves.setUser(user);
		listOfLeaves = new ArrayList<>();
		listOfLeaves.add(leaves);

		userid = 1;
	}

	@Test
	public void getLeaves() throws LeaveException {
		Mockito.when(leaveRepository.findAllByUser_userId(Mockito.anyInt())).thenReturn(listOfLeaves);
		List<Leaves> actual = leaveServiceImpl.getallleave(userid);
		Assert.assertEquals(1, actual.size());
	}

	@Test(expected = LeaveException.class)
	public void getLeavesNegative() throws LeaveException {
		Mockito.when(leaveRepository.findAllByUser_userId(Mockito.anyInt())).thenReturn(listOfLeaves);
		List<Leaves> actual=leaveServiceImpl.getallleave(0);
		Assert.assertNull(actual);
	}

}
