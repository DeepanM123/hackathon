package com.demo.leave.serviceImpl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.assertj.core.api.AssertFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import com.demo.leave.dto.LeaveApplyDto;
import com.demo.leave.entity.Leaves;
import com.demo.leave.entity.User;
import com.demo.leave.enums.LeaveType;
import com.demo.leave.exception.LeaveException;
import com.demo.leave.repository.LeaveApplyRepository;
import com.demo.leave.repository.LeaveRepository;

@RunWith(MockitoJUnitRunner.class)
public class LeaveApplyServiceImplTest {

	@Mock
	LeaveRepository leaveRepository;

	@Mock
	LeaveApplyRepository leaveApplyRepository;

	@InjectMocks
	LeaveApplyServiceImpl leaveApplyServiceImpl;
	
	
	Leaves leaves;
	//Leaves listOfLeaves;
	
	LeaveApplyDto leaveApplyDto;
	LeaveType leaveType;
	int userid;

	User user;

	@Before
	public void setup() {

		leaves = new Leaves();
		
		leaves.setAvailableLeaves(2);
		leaves.setLeaveId(1);
		leaves.setLeaveType(leaveType.Personal);
		leaves.setUser(user);
		
		leaveApplyDto = new LeaveApplyDto();
		leaveApplyDto.setLeaveIdfk(1);
		leaveApplyDto.setLeaveType(leaveType);
		leaveApplyDto.setNumberOfLeaves(5);
		leaveApplyDto.setUserIdfk(1);
		userid=1;
	}
	
	@Test
	public void applyleave() throws LeaveException
	{
		Mockito.when(leaveRepository.findByUser_userIdAndLeaveType(Mockito.anyInt(),Mockito.any())).thenReturn(Optional.of(leaves));
		String actual = leaveApplyServiceImpl.saveLeaveData(leaveApplyDto);
		Assert.assertEquals("Applied for leave successfully",actual);
	}
		
	
	
}