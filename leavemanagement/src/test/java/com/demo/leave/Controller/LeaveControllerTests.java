package com.demo.leave.Controller;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.servlet.handler.UserRoleAuthorizationInterceptor;

import com.demo.leave.dto.LoginDto;
import com.demo.leave.dto.TransactionResponse;
import com.demo.leave.entity.Leaves;
import com.demo.leave.entity.User;
import com.demo.leave.enums.LeaveType;
import com.demo.leave.exception.LeaveException;
import com.demo.leave.repository.UserJpaRepository;
import com.demo.leave.service.LeaveService;

@RunWith(MockitoJUnitRunner.class)
public class LeaveControllerTests {

	@Mock
	LeaveService leaveService;

	@Mock
	UserJpaRepository userRepository;

	@InjectMocks
	LeaveController leaveController;
	User user;
	List<User> user1;
	LoginDto loginDto;
	LeaveType leaveType;
	Leaves leaves;
	List<Leaves> listOfLeaves;
	List<Leaves> listOfLeaves1;
	List<LoginDto> listoflogin;
	List<User> users;
	int userid;
	String username;
	String password;
	TransactionResponse transactionResponse;
	String message;
	int status;
	Set<Leaves> userleaves;

	@Before
	public void Setup() {
		leaves = new Leaves();
		leaves.setAvailableLeaves(11);
		leaves.setLeaveId(1);
		leaves.setLeaveType(leaveType);
		leaves.setUser(user);
		listOfLeaves = new ArrayList<>();
		listOfLeaves.add(leaves);

		
		loginDto = new LoginDto();
		loginDto.setPassword("ok");
		loginDto.setUserName("ddd");
		listoflogin = new ArrayList<>();
		listoflogin.add(loginDto);
		userid = 1;
		username = "dee";
		password = "dee";
		message = "login successful";

		user = new User();
		user.setEmail("deep@gmail.com");
		user.setGender("male");
		user.setLeave(userleaves);
		user.setPassword("dee");
		user.setPhone("6666");
		user.setUserId(1);
		user.setUserName("deee");
		users=new ArrayList<>();
		users.add(user);

	}

	@Test
	public void login() throws LeaveException {

		Mockito.when(userRepository.findByUserNameAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(users);
		String actual = leaveController.loginUser(loginDto);
		Assert.assertEquals("Log In is successfull", actual);

	}
	
	@Test
	public void loginNegative() throws LeaveException {

		user1=new ArrayList<>();
		User user2=new User();
		user1.add(user2);
		LoginDto logindto1=new LoginDto();
		Mockito.when(userRepository.findByUserNameAndPassword(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(user1);
		//String actual = leaveController.loginUser(loginDto);
		assertThrows(LeaveException.class,()->leaveController.loginUser(logindto1));

	}

	@Test
	public void getLeaves() throws LeaveException {

		Mockito.when(leaveService.getallleave(Mockito.anyInt())).thenReturn(listOfLeaves);
		List<Leaves> actualValue = leaveController.getallleavesbyuserid(userid);
		Assert.assertEquals(1, actualValue.size());

	}

	@Test
	public void getLeavesNegative() throws LeaveException {
		

		listOfLeaves1=new ArrayList<>();
		Leaves leaves1=new Leaves();
		listOfLeaves1.add(leaves1);
		int userid=0;
		Mockito.when(leaveService.getallleave(Mockito.anyInt())).thenReturn(listOfLeaves1);
		//leaveController.getallleavesbyuserid(0);
		assertThrows(LeaveException.class,()->leaveController.getallleavesbyuserid(userid));
		
	}

}
