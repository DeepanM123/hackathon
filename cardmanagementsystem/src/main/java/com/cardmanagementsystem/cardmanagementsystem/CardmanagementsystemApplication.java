package com.cardmanagementsystem.cardmanagementsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan("com.cardmanagementsystem.entity")
@EnableJpaRepositories("com.cardmanagementsystem.repository")
@ComponentScan("com.cardmanagementsystem")
@SpringBootApplication
public class CardmanagementsystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(CardmanagementsystemApplication.class, args);
	}

}
