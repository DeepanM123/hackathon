package com.cardmanagementsystem.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.cardmanagement.utility.ApiConstant;

@ControllerAdvice
public class GlobalException extends ResponseEntityExceptionHandler {

	public ResponseEntity<ErrorResponse> error(CardException ex)
	{
		ErrorResponse er=new ErrorResponse();
		er.setMessage(ex.getMessage());
		er.setStatusCode(ApiConstant.RECORD_RECORD_NOT_FOUND_CODE);
		return new ResponseEntity<ErrorResponse>(er,HttpStatus.ACCEPTED);
		
	}
}
