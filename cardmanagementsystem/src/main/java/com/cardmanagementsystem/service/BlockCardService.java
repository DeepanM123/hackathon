package com.cardmanagementsystem.service;

import javax.validation.Valid;

import com.cardmanagementsystem.dto.CustomerBlockRequestDto;
import com.cardmanagementsystem.exception.CardException;

public interface BlockCardService {

	String block(@Valid CustomerBlockRequestDto customerBlockRequestDto) throws CardException;


	

}
