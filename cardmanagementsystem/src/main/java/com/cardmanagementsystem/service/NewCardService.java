package com.cardmanagementsystem.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;


import com.cardmanagementsystem.dto.CustomerNewCardRequestDto;
import com.cardmanagementsystem.dto.NewCardDto;
import com.cardmanagementsystem.entity.CardDetails;
import com.cardmanagementsystem.exception.CardException;

public interface NewCardService {

	List<NewCardDto> raiseNewCard(@Valid CustomerNewCardRequestDto customerCardRequestDto) throws CardException;

}
