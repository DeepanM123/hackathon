package com.cardmanagementsystem.dto;

public class CustomerBlockRequestDto {
	private int CustomerId;
	private String RequestType;
	private String RequestReason;
	private String CardNumber;
	public int getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(int customerId) {
		CustomerId = customerId;
	}
	public String getRequestType() {
		return RequestType;
	}
	public void setRequestType(String requestType) {
		RequestType = requestType;
	}
	public String getRequestReason() {
		return RequestReason;
	}
	public void setRequestReason(String requestReason) {
		RequestReason = requestReason;
	}
	public String getCardNumber() {
		return CardNumber;
	}
	public void setCardNumber(String cardNumber) {
		CardNumber = cardNumber;
	}
	public CustomerBlockRequestDto(int customerId, String requestType, String requestReason, String cardNumber) {
		super();
		CustomerId = customerId;
		RequestType = requestType;
		RequestReason = requestReason;
		CardNumber = cardNumber;
	}
	public CustomerBlockRequestDto() {
		super();
	}
	

}
