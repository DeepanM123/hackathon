package com.cardmanagementsystem.dto;

import java.util.Date;

public class CardDetailsDto {

	private String cardNumber;
	private Date cardExpiriyDate;
	private int cardCvv;
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public Date getCardExpiriyDate() {
		return cardExpiriyDate;
	}
	public void setCardExpiriyDate(Date cardExpiriyDate) {
		this.cardExpiriyDate = cardExpiriyDate;
	}
	public int getCardCvv() {
		return cardCvv;
	}
	public void setCardCvv(int cardCvv) {
		this.cardCvv = cardCvv;
	}
	public CardDetailsDto(String cardNumber, Date cardExpiriyDate, int cardCvv) {
		super();
		this.cardNumber = cardNumber;
		this.cardExpiriyDate = cardExpiriyDate;
		this.cardCvv = cardCvv;
	}
	public CardDetailsDto() {
		super();
	}

	
}
