package com.cardmanagementsystem.dto;

public class CustomerNewCardRequestDto {
	
	private int CustomerId;
	private String RequestType;
	private String RequestReason;
	public int getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(int customerId) {
		CustomerId = customerId;
	}
	public String getRequestType() {
		return RequestType;
	}
	public void setRequestType(String requestType) {
		RequestType = requestType;
	}
	public String getRequestReason() {
		return RequestReason;
	}
	public void setRequestReason(String requestReason) {
		RequestReason = requestReason;
	}
	
	public CustomerNewCardRequestDto(int customerId, String requestType, String requestReason) {
		super();
		CustomerId = customerId;
		RequestType = requestType;
		RequestReason = requestReason;
	}
	public CustomerNewCardRequestDto() {
		super();
	}
	
	
}
