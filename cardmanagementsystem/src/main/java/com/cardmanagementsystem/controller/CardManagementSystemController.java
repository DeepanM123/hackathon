package com.cardmanagementsystem.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cardmanagementsystem.dto.CustomerBlockRequestDto;
import com.cardmanagementsystem.dto.CustomerNewCardRequestDto;
import com.cardmanagementsystem.dto.NewCardDto;
import com.cardmanagementsystem.entity.CardDetails;
import com.cardmanagementsystem.exception.CardException;
import com.cardmanagementsystem.service.BlockCardService;
import com.cardmanagementsystem.service.NewCardService;
import com.cardmanagementsystem.service.ReissueCardService;

@RestController
public class CardManagementSystemController {

	@Autowired
	NewCardService newCardService;

	@Autowired
	BlockCardService blockCardService;

	@Autowired
	ReissueCardService reissueCardService;

	@PostMapping("/newCard")
	public List<NewCardDto> raiseNewCard(@Valid @RequestBody CustomerNewCardRequestDto customerCardRequestDto) throws CardException {
		return newCardService.raiseNewCard(customerCardRequestDto);

	}
	
	@PostMapping("/blockCard")
	public ResponseEntity<String> blockCard(@Valid @RequestBody CustomerBlockRequestDto customerBlockRequestDto) throws CardException
	{
		return new ResponseEntity<String>(blockCardService.block(customerBlockRequestDto),HttpStatus.ACCEPTED);
	}

}
