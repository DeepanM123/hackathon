package com.cardmanagementsystem.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cardmanagementsystem.entity.CardDetails;

@Repository
public interface BlockCardRepository extends JpaRepository<CardDetails, Integer> {

	Optional<CardDetails> findByCustomerDetails_customerIdAndRequestTypeAndRequestReasonAndCardNumber(int customerId,
			String requestType, String requestReason, String cardNumber);

}
