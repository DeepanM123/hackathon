package com.cardmanagementsystem.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cardmanagementsystem.entity.CardDetails;

@Repository
public interface NewCardRepository extends JpaRepository<CardDetails,Integer> {

	Optional<CardDetails> findByCustomerDetails_customerIdAndRequestType(int customerId, String requestType);

	List<CardDetails> findByCustomerDetails_customerIdAndCardCvvAndCardExpiriyDateAndCardNumber(int customerId, int cvv, Date expiriydate, String cardnumber);

}
