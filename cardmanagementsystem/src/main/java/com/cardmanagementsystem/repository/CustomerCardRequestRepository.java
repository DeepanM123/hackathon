package com.cardmanagementsystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cardmanagementsystem.entity.CustomerCardRequest;

@Repository
public interface CustomerCardRequestRepository extends JpaRepository<CustomerCardRequest, Integer> {

}
