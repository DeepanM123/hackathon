package com.cardmanagementsystem.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class CustomerDetails implements Serializable {

	@Id
	@GeneratedValue
	private int customerId;
	private String customerName;
	private String customerEmail;
	private long customerPhonenumber;
	private Date customerDateOfBirth;
	private String customerGender;

	public CustomerDetails() {
		super();
	}

	
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public long getCustomerPhonenumber() {
		return customerPhonenumber;
	}

	public void setCustomerPhonenumber(long customerPhonenumber) {
		this.customerPhonenumber = customerPhonenumber;
	}

	public Date getCustomerDateOfBirth() {
		return customerDateOfBirth;
	}

	public void setCustomerDateOfBirth(Date customerDateOfBirth) {
		this.customerDateOfBirth = customerDateOfBirth;
	}

	public String getCustomerGender() {
		return customerGender;
	}

	public void setCustomerGender(String customerGender) {
		this.customerGender = customerGender;
	}


	@OneToMany(cascade = CascadeType.ALL, mappedBy = "customerDetails")
	Set<CardDetails> cardDetails;

	public CustomerDetails(Set<CardDetails> cardDetails) {
		super();
		this.cardDetails = cardDetails;
	}

	public Set<CardDetails> getCardDetails() {
		return cardDetails;
	}

	public void setCardDetails(Set<CardDetails> cardDetails) {
		this.cardDetails = cardDetails;
	}
	
	

}
