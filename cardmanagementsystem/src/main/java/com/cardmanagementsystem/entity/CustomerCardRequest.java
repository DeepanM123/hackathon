package com.cardmanagementsystem.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CustomerCardRequest implements Serializable {
	@Id
	@GeneratedValue
	private int customerCardRequestId;
	private int CustomerId;
	private String RequestType;
	private String RequestReason;
	private String CardNumber;
	public CustomerCardRequest() {
		super();
	}
	public int getCustomerCardRequestId() {
		return customerCardRequestId;
	}
	public void setCustomerCardRequestId(int customerCardRequestId) {
		this.customerCardRequestId = customerCardRequestId;
	}
	public int getCustomerId() {
		return CustomerId;
	}
	public void setCustomerId(int customerId) {
		CustomerId = customerId;
	}
	public String getRequestType() {
		return RequestType;
	}
	public void setRequestType(String requestType) {
		RequestType = requestType;
	}
	public String getRequestReason() {
		return RequestReason;
	}
	public void setRequestReason(String requestReason) {
		RequestReason = requestReason;
	}
	public String getCardNumber() {
		return CardNumber;
	}
	public void setCardNumber(String cardNumber) {
		CardNumber = cardNumber;
	}


}
