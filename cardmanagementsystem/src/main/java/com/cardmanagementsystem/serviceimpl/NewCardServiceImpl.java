package com.cardmanagementsystem.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cardmanagement.utility.ApiConstant;
import com.cardmanagementsystem.dto.CardDetailsDto;
import com.cardmanagementsystem.dto.CustomerNewCardRequestDto;
import com.cardmanagementsystem.dto.NewCardDto;
import com.cardmanagementsystem.entity.CardDetails;
import com.cardmanagementsystem.exception.CardException;
import com.cardmanagementsystem.repository.NewCardRepository;
import com.cardmanagementsystem.service.NewCardService;

@Service
public class NewCardServiceImpl implements NewCardService {

	@Autowired
	NewCardRepository newCardRepository;

	@Override
	public List<NewCardDto> raiseNewCard(@Valid CustomerNewCardRequestDto customerCardRequestDto) throws CardException {
		CardDetails cardDetailss;
		List<NewCardDto> newCardDto = new ArrayList<>();
		Optional<CardDetails> cardDetails = newCardRepository.findByCustomerDetails_customerIdAndRequestType(
				customerCardRequestDto.getCustomerId(), customerCardRequestDto.getRequestType());
		Integer cvv = new Integer(123);
		Date date = new Date("2222-12-20");
		String cardnumber = new String("1233-1344-1234");
		List<CardDetails> CardDetailsList = newCardRepository
				.findByCustomerDetails_customerIdAndCardCvvAndCardExpiriyDateAndCardNumber(
						customerCardRequestDto.getCustomerId(), cvv, date, cardnumber);
		if (!cardDetails.isPresent()) {
			throw new CardException(ApiConstant.RECORD_NOT_FOUND);
		}
		if (cardDetails.get().getRequestType() == customerCardRequestDto.getRequestType()) {
			for (CardDetails cardDetail : CardDetailsList) {
				NewCardDto newCardDtolist = new NewCardDto();
				newCardDtolist.setCardCvv(cardDetail.getCardCvv());
				newCardDtolist.setCardExpiriyDate(cardDetail.getCardExpiriyDate());
				newCardDtolist.setCardNumber(cardDetail.getCardNumber());
				newCardDto.add(newCardDtolist);
			}
		}

		return newCardDto;
	}

}
