package com.cardmanagementsystem.serviceimpl;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cardmanagement.utility.ApiConstant;
import com.cardmanagementsystem.dto.CustomerBlockRequestDto;
import com.cardmanagementsystem.entity.CardDetails;
import com.cardmanagementsystem.exception.CardException;
import com.cardmanagementsystem.repository.BlockCardRepository;
import com.cardmanagementsystem.service.BlockCardService;

@Service
public class BlockCardServiceImpl implements BlockCardService {
	@Autowired
	BlockCardRepository blockCardRepository;

	@Override
	public String block(@Valid CustomerBlockRequestDto customerBlockRequestDto) throws CardException {
		Optional<CardDetails> cardlist = blockCardRepository
				.findByCustomerDetails_customerIdAndRequestTypeAndRequestReasonAndCardNumber(
						customerBlockRequestDto.getCustomerId(), customerBlockRequestDto.getRequestType(),
						customerBlockRequestDto.getRequestReason(), customerBlockRequestDto.getCardNumber());
		if(!cardlist.isPresent())
		{
			throw new CardException(ApiConstant.RECORD_NOT_FOUND);
		}
		return ApiConstant.CARD_BLOCKED;

	}

}
