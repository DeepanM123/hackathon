package com.cardmanagement.utility;

public class ApiConstant {
	
	public static final String RECORD_NOT_FOUND="Record not found";
	public static final int RECORD_RECORD_NOT_FOUND_CODE=700;
	public static final String CARD_BLOCKED="Card Has Been Blocked";
}
